#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "OpenDoor.generated.h"

class ATriggerVolume;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	void SetupAudioComponent();
	void CheckPressurePlate();
	void OpenDoor(float DeltaTime, float Speed);
	void CloseDoor(float DeltaTime, float Speed);
	float TotalMassOfActors() const;

	float InitialYaw;
	float LastOpened = 0;

	UPROPERTY(EditAnywhere)
	float OpenAngle = 90;

	UPROPERTY(EditAnywhere)
	float CloseDelay = 1;

	UPROPERTY(EditAnywhere)
	float OpenSpeed = 1;

	UPROPERTY(EditAnywhere)
	float CloseSpeed = 1;

	UPROPERTY()
	UAudioComponent* AudioComponent = nullptr;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate;

	UPROPERTY(EditAnywhere)
	float TriggerMass = 50;

	bool OpenSound = false;
	bool CloseSound = true;
};
