#include "OpenDoor.h"

#include "Components/AudioComponent.h"
#include "Engine/TriggerVolume.h"
#include "GameFramework/Actor.h"
#include "GameFrameWork/PlayerController.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	/*UE_LOG(LogTemp, Warning, TEXT("Current Yaw: %f"), GetOwner()->GetActorRotation().Yaw);*/
	if (TotalMassOfActors() > TriggerMass)
	{
		OpenDoor(DeltaTime, OpenSpeed);
		LastOpened = GetWorld()->GetTimeSeconds();
	}
	else
	{
		if (GetWorld()->GetTimeSeconds() - LastOpened > CloseDelay)
		{
			CloseDoor(DeltaTime, CloseSpeed);
		}
	}
}

// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	SetupAudioComponent();
	CheckPressurePlate();

	InitialYaw = GetOwner()->GetActorRotation().Yaw;
	OpenAngle += InitialYaw;
}

void UOpenDoor::SetupAudioComponent()
{
	AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
	if (AudioComponent)
	{
		UE_LOG(LogTemp, Display, TEXT("Audio component found on %s. Audio correctly set up"), *GetOwner()->GetName());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No audio component found on %s. Audio component can't work"), *GetOwner()->GetName());
	}
}

void UOpenDoor::CheckPressurePlate()
{
	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("%s has no pressure plate set"), *GetOwner()->GetName());
	}
}

void UOpenDoor::OpenDoor(float DeltaTime, float Speed)
{
	FRotator OpenDoor = GetOwner()->GetActorRotation();
	OpenDoor.Yaw = FMath::FInterpTo(OpenDoor.Yaw, OpenAngle, DeltaTime, Speed);
	GetOwner()->SetActorRotation(OpenDoor);

	if (AudioComponent && !OpenSound)
	{
		AudioComponent->Play();
		OpenSound = true;
	}
	CloseSound = false;
}

void UOpenDoor::CloseDoor(float DeltaTime, float Speed)
{
	FRotator OpenDoor = GetOwner()->GetActorRotation();
	OpenDoor.Yaw = FMath::FInterpTo(OpenDoor.Yaw, InitialYaw, DeltaTime, Speed);
	GetOwner()->SetActorRotation(OpenDoor);

	if (AudioComponent && !CloseSound)
	{
		AudioComponent->Play();
		CloseSound = true;
	}
	OpenSound = false;
}

float UOpenDoor::TotalMassOfActors() const
{
	TArray<AActor*> OverlappingActors;

	if (!PressurePlate)
	{
		return 0;
	}
	PressurePlate->GetOverlappingActors(OverlappingActors);
	float TotalMass = 0;
	for (auto const& Actor : OverlappingActors)
	{
		TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	return TotalMass;
}

